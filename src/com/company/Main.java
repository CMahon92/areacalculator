package com.company;
public class Main {

    public static void main(String[] args) {

        area(43.00d);
        area(22.00d, 7.23d);
        area(-0.1d);
        area(-1.00d, 3.47d);
    }

    public static double area (double radius) {
        if (radius < 0) {
            System.out.println("-1.0");
            return -1.0;
        }
        double circle = Math.PI * (Math.pow(radius,2));
        System.out.println("A circle's radius of " + radius + " give us an area of " + circle);
        return circle;
    }
    public static double area (double x, double y) {
        if (x < 0 || y < 0 ) {
            System.out.println("-1.0");
            return -1.0;
        }
        double rectangle = x * y;
        System.out.println("The area of the rectangle is " + rectangle);
        return rectangle;
    }
}
